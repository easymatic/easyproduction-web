import { createAction } from 'redux-actions';
import axios from "axios";

import rest from "../utils/rest";

export const getTokenRequest = createAction('GET_TOKEN_REQUEST');
export const getTokenSuccess = createAction('GET_TOKEN_SUCCESS');
export const getTokenFailure = createAction('GET_TOKEN_FAILURE');

export const login = (username, password) => async (dispatch) => {
  dispatch(getTokenRequest());
  try {
    const data = new FormData();
    data.append('username', username);
    data.append('password', password);
    const response = await rest.post('/api-token-auth/', data);
    const token = response.data.token;
    localStorage.setItem('auth_token', token);
    axios.defaults.headers.common['Authorization'] = `Token ${token}`;
    dispatch(getTokenSuccess({ token }));
  } catch (e) {
    dispatch(getTokenFailure(e.toString()));
  }
};

export const logoutSuccess = createAction('LOGOUT_SUCCESS');

export const logout = () => async (dispatch) => {
  localStorage.removeItem('auth_token');
  window.location.reload();
  dispatch(logoutSuccess());
};
