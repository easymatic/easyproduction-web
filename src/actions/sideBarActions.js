import { createAction } from 'redux-actions';

export const updateSideBarSuccess = createAction('UPDATE_SIDEBAR_SUCCESS');

export const updateSideBar = (child) => async (dispatch) => {
  dispatch(updateSideBarSuccess({ child }));
};
