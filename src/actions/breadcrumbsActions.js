import { createAction } from 'redux-actions';

export const updateBreadcrumbsSuccess = createAction('UPDATE_BREADCRUMBS_SUCCESS');

export const updateBreadcrumbs = () => async (dispatch) => {
  dispatch(updateBreadcrumbsSuccess({}));
};
