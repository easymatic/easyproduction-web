import { createAction } from 'redux-actions';

import rest from "../utils/rest";

export const getUsersRequest = createAction('GET_USERS_REQUEST');
export const getUsersSuccess = createAction('GET_USERS_SUCCESS');
export const getUsersFailure = createAction('GET_USERS_FAILURE');

export const getUsers = () => async (dispatch) => {
  dispatch(getUsersRequest());
  try {
    const response = await rest.get('api/v1/users/');
    console.log('getUsers. data: ', response.data);
    dispatch(getUsersSuccess(response.data));
  } catch (e) {
    dispatch(getUsersFailure(e.toString()));
  }
};

