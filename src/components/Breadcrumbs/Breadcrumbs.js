import React from 'react';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import classNames from 'classnames'

import logo from "../../views/Auth/logo.png";


const styles = theme => ({
  breadcrumbs: {
    display: 'flex',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  logo: {
    maxWidth: '400px'
  }
});


const Breadcrumbs = props => {

    const { classes, height } = props;

    return (
      <div className={classes.breadcrumbs} style={{height: height}}>
        <img src={logo} alt="easyproduction" className={classNames('img-responsive', classes.logo)}/>
      </div>
    )
};

Breadcrumbs.propTypes = {
  classes: PropTypes.object.isRequired,
  height: PropTypes.string.isRequired
};

const styledBreadcrumbs = withStyles(styles)(Breadcrumbs);

const mapStateToProps = () => {
  return {};
};

const BreadcrumbsContainer = connect(
  mapStateToProps
)(styledBreadcrumbs);

export default BreadcrumbsContainer;
