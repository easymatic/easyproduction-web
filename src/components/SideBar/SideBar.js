import React, { Component } from 'react';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withStyles, Drawer, Divider, Button } from "@material-ui/core";

import * as authActions from "../../actions/authActions";

const styles = theme => ({
  drawer: {
    flexShrink: 0,
    display: 'flex'
  },
  drawerPaper: {
    backgroundColor: 'inherit',
    overflowX: 'hidden'
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    flexGrow: '1'
  },
  childWrapper: {
    flexGrow: '1',
    padding: '16px'
  }
});


class SideBar extends Component {

  logout = (e) => {
    authActions.logout()();
  };

  render() {

    console.log('SideBar component. render. this.props: ', this.props);

    const { classes, child, headerHeight, width } = this.props;

    return (
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{paper: classes.drawerPaper}}
        style={{width: width}}
      >
        <div className={classes.toolbar} style={{height: headerHeight, width: width}}>
          user info
        </div>
        <Divider/>
        <div className={classes.content} style={{width: width}}>
          <div className={classes.childWrapper}>
            {child}
          </div>
          <Button onClick={this.logout}>logout</Button>
        </div>
      </Drawer>
    )
  }
}

SideBar.propTypes = {
  classes: PropTypes.object.isRequired,
  child: PropTypes.node,
  width: PropTypes.string.isRequired,
  headerHeight: PropTypes.string.isRequired
};

const styledSideBar = withStyles(styles)(SideBar);

const mapStateToProps = ({ sideBarChild: { child } }) => {
  return { child };
};

const SideBarContainer = connect(
  mapStateToProps
)(styledSideBar);

export default SideBarContainer;
