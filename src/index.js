import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";

import * as serviceWorker from './serviceWorker';
import rootReducer from './reducers';
import PrivateRoute from "./views/PrivateRoute";
import App from './views/App/App';
import Login from "./views/Auth/Login";

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk),
    // devtoolMiddleware,
  ),
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/login" component={Login}/>
        <PrivateRoute path="/" component={App} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
