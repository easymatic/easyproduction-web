const _namedRoutes = {
  'login': {
    path: '/login',
    reverse(){
      return this.path
    }
  },
  'users': {
    path: '/users',
    reverse(){
      return this.path
    }
  },
};

const namedRoutes = Object.keys(_namedRoutes).reduce((acc, item) => {
  const route = _namedRoutes[item];
  route.exact = true;
  const nameVariants = item.split('|');

  for(let name in nameVariants){
    acc[nameVariants[name]] = route
  }

  return acc;
}, {});

export const getRoute = (name) => {
  const route = namedRoutes[name];

  if( !route ){
    throw new Error(`Route with name "${name}" not found.`)
  }

  return route
};
