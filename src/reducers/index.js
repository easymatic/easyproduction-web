import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { loginReducer } from './authReducer';
import { usersReducer } from "./usersReducer";
import { sideBarReducer } from "./sideBarReducer";
import { breadcrumbsReducer } from "./breadcrumbsReducer";

export default combineReducers({
  auth: loginReducer,
  sideBarChild: sideBarReducer,
  breadcrumbsItems: breadcrumbsReducer,
  users: usersReducer,
  form: formReducer,
})
