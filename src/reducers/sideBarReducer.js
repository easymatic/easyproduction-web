import { handleActions } from 'redux-actions';

import * as actions from '../actions/sideBarActions';

const initialState = {
  child: null
};

export const sideBarReducer = handleActions({
  [actions.updateSideBarSuccess](state, { payload: { child } }) {
    return { child }
  }
}, initialState);
