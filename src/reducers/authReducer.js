import { handleActions } from 'redux-actions';

import * as actions from '../actions/authActions';

const initialState = {
  token: null,
  loading: false,
  error: null,
};

export const loginReducer = handleActions({
  [actions.getTokenRequest](state) {
    return {...state, loading: true}
  },
  [actions.getTokenSuccess](state, { payload }) {
    return { token: payload.token, loading: false, error: null };
  },
  [actions.getTokenFailure](state, { payload }) {
    return { ...initialState, error: payload };
  },
  [actions.logoutSuccess](state) {
    return { ...initialState }
  },
}, initialState);
