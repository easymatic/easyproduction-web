import { handleActions } from 'redux-actions';

import * as actions from '../actions/breadcrumbsActions';

export const breadcrumbsReducer = handleActions({
  [actions.updateBreadcrumbsSuccess](state, { payload: items }) {
    return items
  }
}, []);