import { handleActions } from 'redux-actions';

import * as actions from '../actions/usersActions';

const initialState = {
  data: {},
  loading: false,
  error: null,
};

export const usersReducer = handleActions({
  [actions.getUsersRequest](state) {
    return {...initialState, loading: true}
  },
  [actions.getUsersSuccess](state, { payload }) {
    return { data: payload, loading: false, error: null };
  },
  [actions.getUsersFailure](state, { payload }) {
    return { ...initialState, error: payload };
  }
}, initialState);
