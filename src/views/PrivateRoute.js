import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { getRoute } from "../namedRoutes";

class PrivateRoute extends Component {

  render() {
    // console.log('PrivateRoute component. render.props: ', this.props);

    const { component: Component, ...rest } = this.props;
    const token = localStorage.getItem('auth_token');

    return (
      <Route {...rest} render={props => {
        // console.log('props: ', props);
        return (token || token) ? (
          <Component {...props}/>
        ) : (
          <Redirect to={{
            pathname: getRoute('login').path,
            state: {from: props.location}
          }}
          />
        )
      }
      }/>
    );
  }
}

export default PrivateRoute;
