import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { MoreVert } from '@material-ui/icons';

import * as usersActions from '../../actions/usersActions';
import objectIsEmpty from "../../utils/objectIsEmpty";

const styles = theme => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    padding: '16px',
    flexGrow: '1'
  },
  list: {
    display: 'flex',
    flexDirection: 'column'
  },
  row: {
    boxShadow: '4px 4px 16px -4px grey',
    marginBottom: '8px',
    display: 'flex'
  },
  name: {
    flexGrow: '6',
    padding: '24px 0',
  },
  email: {
    flexGrow: '10',
    padding: '24px 0',
  },
  role: {
    flexGrow: '10',
    padding: '24px 0',
  },
  ctrl: {
    flexGrow: '1',
    margin: 'auto',
    display: 'flex',
    justifyContent: 'center'
    // backgroundColor: 'grey'
  },

});

class Users extends Component {

  componentDidMount() {
    this.props.getUsers();
  }

  render() {
    console.log('Users component. render. this.props: ', this.props);
    const { classes, loading, users, error } = this.props;

    if(!!error){
      return <h1>Ooops!! Error: {error}</h1>
    }

    if(loading || objectIsEmpty(users)){
      return null
    }

    return (
      <div className={classes.wrapper}>
        <h1>Users list</h1>
        {!objectIsEmpty(users) && <div className={classes.list}>
          {users.results.map(user => {
            return (
              <div key={user.email} className={classes.row}>
                <span className={classes.name}>
                  name: "{user.first_name} {user.last_name}"
                </span>
                <span className={classes.email}>
                  Email: {user.email}
                </span>
                <span className={classes.role}>
                  Role: {user.role}
                </span>
                <div className={classes.ctrl}>
                  <MoreVert/>
                </div>
              </div>
            )
          })}
        </div>}
      </div>
    );
  }
}

Users.propTypes = {
  classes: PropTypes.object.isRequired,
};
const styledUsers = withStyles(styles)(Users);

const mapStateToProps = ({ users: { loading, data, error } }) => ({
  loading,
  users: data,
  error
});

const UsersContainer = connect(
  mapStateToProps,
  {
    getUsers: usersActions.getUsers,
  },
)(styledUsers);

export default UsersContainer;
