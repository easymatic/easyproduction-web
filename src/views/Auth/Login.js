import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form';
import { Redirect } from 'react-router-dom';
import { withStyles, Grid } from "@material-ui/core";

import * as authActions from "../../actions/authActions";
import logo from'./logo.png';
import css from'./css.css';

const validate = values => {
  const errors = {};
  const requiredFields = [
    'username',
    'password',
  ];
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  });
  return errors
};

const styles = theme => ({
  grid: {
    height: '100vh'
  },
  wrapper: {
    width: '600px'
  },
  logoWrapper: {
    marginBottom: '64px'
  },
  form: {
    display: 'flex',
    'justify-content': 'center',
    'flex-direction': 'column'
  },
  field: {
    display: 'flex',
    height: '50px',
    marginTop: '16px',
    color: '#a1a1a1',
    fontSize: '1.4em',
    '&:focus-within > span': {
      color: '#29abe2',
    },
    '& > span': {
      lineHeight: '50px',
      flexGrow: '0',
      flexShrink: '0',
      width: '160px',
      verticalAlign: 'middle',
    },
    '& > div': {
      flexGrow: '1',
      flexShrink: '1',
      '& > input': {
        width: 'calc(100% - 160px)',
        padding: '0 16px',
        border: 'none',
        backgroundColor: 'white',
        borderLeft: '2px solid #e6e6e6',
        lineHeight: '50px',
        '&:focus': {
          borderLeft: '2px solid #29abe2',
        }
      },
      '&:focus-within': {
        boxShadow: '0 15px 15px -10px #e6e6e6'
      }
    }
  },
  button: {
    display: 'flex',
    flexDirection: 'row-reverse',
    marginTop: '32px',
    '& > button': {
      backgroundColor: '#29abe2',
      width: '150px',
      height: '50px',
      color: 'white',
      border: 'none',
      fontSize: '1.4em'
    }
  }
});


class Login extends Component {

  login = ({ username, password}) => {
    // console.log('username', username, 'password', password);
    this.props.login(username, password);
    this.props.reset();
  };

  render() {

    // console.log('LoginForm component. render. this.props: ', this.props);

    const { from } = this.props.location.state || { from: { pathname: '/' } };
    console.log('LoginForm component. from: ', from);
    const { loading, classes } = this.props;
    const token = localStorage.getItem('auth_token');

    if (token) {
      return (
        <Redirect to={from}/>
      )
    }

    return (
      <Grid container direction="column" justify="center" alignItems="center" className={classes.grid}>
        <div className={classes.wrapper}>
          <div className={classes.logoWrapper}>
            <img src={logo} alt="easyproduction" className="img-responsive"/>
          </div>
          <div>
            <form
              // className={classes.form}
              onSubmit={this.props.handleSubmit(this.login)}
            >
              <label className={classes.field}>
                <span>email</span>
                <div>
                  <Field
                    type="text"
                    name="username"
                    label="username"
                    component={'input'}
                  />
                </div>
              </label>
              <label className={classes.field}>
                <span>password</span>
                <div>
                  <Field
                    type="password"
                    name="password"
                    label="password"
                    component={'input'}
                  />
                </div>
              </label>
              <div className={classes.button}>
                <button
                  type="submit"
                  disabled={loading}
                >log in</button>
              </div>
            </form>
          </div>
        </div>
      </Grid>
    )
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

const LoginForm = reduxForm({
  form: 'loginForm',
  validate
})(Login);

const StyledLoginForm = withStyles(styles)(LoginForm);

const mapStateToProps = ({ auth: { token, loading }}) => {
  return {
    loading: loading
  };
};

export default connect(
  mapStateToProps,
  { login: authActions.login },
)(StyledLoginForm);
