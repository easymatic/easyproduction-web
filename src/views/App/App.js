import React, { Component } from 'react';
import PropTypes from "prop-types";
import { Redirect, Route, Switch } from "react-router-dom";
import { withStyles, CssBaseline,  } from "@material-ui/core";

import { getRoute } from "../../namedRoutes";
import SideBar from "../../components/SideBar/SideBar";
import Breadcrumbs from "../../components/Breadcrumbs/Breadcrumbs";
import Users from "../Users/Users";

const headerHeight = '88px';
const sideBarWidth = '350px';

const styles = theme => ({
  root: {
    display: 'flex'
  },
  content: {
    flexGrow: 1,
    overflow: 'hidden'
  },
  contentWrapper: {
    overflow: 'hidden',
    height: '100vh',
    display: 'flex',
    flexDirection: 'column'
  },
  breadcrumbs: {
    // height: `${drawerHeaderHeight}px`,
    flexShrink: 0,
    borderBottom: 'solid 1px rgba(0, 0, 0, 0.12)',
    paddingLeft: '16px',
    paddingRight: '16px'
  },
  workspace: {
    flexGrow: 1,
    overflow: 'auto',
    display: 'flex'
  }
});


class App extends Component {

  render() {

    console.log('App component. render. this.props: ', this.props);
    const { classes } = this.props;

    return (

      <div className={classes.root}>
        <CssBaseline  />
        <SideBar headerHeight={headerHeight} width={sideBarWidth}/>
        <main className={classes.content}>
          <div className={classes.contentWrapper}>
            <div className={classes.breadcrumbs}>
              <Breadcrumbs height={headerHeight}/>
            </div>
            <div className={classes.workspace}>
              <Switch>
                <Route exact path='/'>
                  <Redirect to={getRoute('users').reverse()} />
                </Route>
                <Route {...getRoute('users')} component={Users}/>
              </Switch>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(App);
